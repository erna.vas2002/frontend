import { useContext } from "react";
import { CartProductItem } from "../cartProductItem/CartProductItem";
import { CartContext } from "../../../../context/CartContext/CartContext";

export const CartProductsList = () => {
  const { state: cartProductsState } = useContext(CartContext);

  return (
    <div className="cart-list-product">
      {cartProductsState.productsInCart.length ? (
        cartProductsState.productsInCart.map((elem, index) => (
          <CartProductItem
            key={index + 1}
            count={elem.count}
            productCart={elem.productInCart}
          />
        ))
      ) : (
        <div className="empty-cart-text">Корзина пуста</div>
      )}
    </div>
  );
};
