import { useContext } from "react";
import { productType } from "../../../../types/GoodsInCartType";
import { CartContext } from "../../../../context/CartContext/CartContext";
import { incrementProducts } from "../../../../context/CartContext/CartActions";
import { decrementProducts } from "../../../../context/CartContext/CartActions";
import { deleteProductCart } from "../../../../context/CartContext/CartActions";
import { MdOutlineCurrencyRuble } from "react-icons/md";

interface productProps {
  productCart: productType;
  count: number;
}

export const CartProductItem: React.FC<productProps> = ({
  productCart,
  count,
}) => {
  const { dispatch } = useContext(CartContext);

  const deleteGood = () => {
    dispatch(deleteProductCart(productCart.id));
  };

  const decrement = () => {
    dispatch(decrementProducts(productCart.id));

    if (count === 1) {
      deleteGood();
    }
  };

  const increment = () => {
    dispatch(incrementProducts(productCart.id));
  };

  return (
    <div className="cart-product">
      <div className="product-info">
        <img src={`${productCart.img}`} alt={productCart.title} />
        <div className="product-detail">
          <span className="cart-name">{productCart.title}</span>
          <div className="product-price">
            <span>{productCart.price}</span>
            <MdOutlineCurrencyRuble />
          </div>
        </div>
      </div>
      <div className="product-count">
        <div className="container-btn">
          <button onClick={decrement} className="btn-decrement">
            <span>-</span>
          </button>
          <span className="product-count">{count}</span>
          <button onClick={increment} className="btn-increment">
            <span>+</span>
          </button>
        </div>
        <div className="product-price black">
          <span>{productCart.price}</span>
          <MdOutlineCurrencyRuble />
        </div>
      </div>
      <button onClick={deleteGood} className="btn-delete">
        <img src="src\assets\icons\delete.svg" alt="удалить" />
      </button>
    </div>
  );
};
