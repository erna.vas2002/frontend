import { CartProductsList } from "./components/cartProductsList/CartProductsList";
import { useContext } from "react";
import { CartContext } from "../../context/CartContext/CartContext";

export const CartGoods = () => {
  const { state: cartProductsState } = useContext(CartContext);

  const getCountProducts = () => {
    return cartProductsState.productsInCart.reduce(
      (acc, cur) => acc + cur.count,
      0
    );
  };

  const sumProducts = () => {
    return cartProductsState.productsInCart.reduce(
      (acc, cur) => acc + cur.count * cur.productInCart.price,
      0
    );
  };

  return (
    <div className="cart-container">
      <h3 className="cart-title">Корзина</h3>
      <div className="cart-wrap">
        <CartProductsList />
        {getCountProducts() > 0 && (
          <div className="payment-container">
            <div className="total-cost">
              <span>Итого</span>
              <span>{sumProducts()}</span>
            </div>
            <button className="checkout-button">Перейти к оформлению</button>
          </div>
        )}
      </div>
    </div>
  );
};
