import { useContext } from "react";
import { GoodsItem } from "../GoodsItem/GoodsItem";
import { GoodsContext } from "../../../../context/GoodsContext/GoodsContext";

interface GoodsListProps {
  type: string;
}

export const GoodsList: React.FC<GoodsListProps> = ({ type }) => {
  const { state: goodsState } = useContext(GoodsContext);

  return (
    <div className="goods-list">
      {goodsState.goods.HeadphoneCategory[type].map((good) => (
        <GoodsItem key={good.id} good={good} />
      ))}
    </div>
  );
};
