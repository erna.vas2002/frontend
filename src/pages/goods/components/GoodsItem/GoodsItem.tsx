import {
  addProductCart,
  incrementProducts,
} from "../../../../context/CartContext/CartActions";
import { CartContext } from "../../../../context/CartContext/CartContext";
import { Headphone } from "../../../../types/GoodsType";
import { FC, useContext } from "react";
import { MdOutlineCurrencyRuble } from "react-icons/md";

interface GoodsItemProps {
  good: Headphone;
}

export const GoodsItem: FC<GoodsItemProps> = ({ good }) => {
  const { dispatch, state: cartProductsState } = useContext(CartContext);

  const addProduct = (e: any) => {
    const cart = document.querySelector(".cart");

    if (
      cartProductsState.productsInCart.some(
        (p) => p.productInCart.id === good.id
      )
    ) {
      dispatch(incrementProducts(good.id));
    } else {
      dispatch(addProductCart(good));
      dispatch(incrementProducts(good.id));
    }

    const productBlock = e.target.closest(".goods-item"); // блок товара, на который кликнули
    const productCoordinates = productBlock.getBoundingClientRect(); // координаты блока товара
    const cartCoordinates = cart?.getBoundingClientRect(); // координаты корзины

    const clonedProduct = productBlock.cloneNode(true); // копия блока товара
    clonedProduct.style.position = "absolute";
    clonedProduct.style.left = productCoordinates.left + "px";
    clonedProduct.style.top = productCoordinates.top + "px";
    clonedProduct.style.opacity = "0.5";
    clonedProduct.style.transition = "all 0.5s ease-in-out";

    document.body.appendChild(clonedProduct); // добавляем копию на страницу

    // Анимация перемещения товара к корзине
    setTimeout(() => {
      clonedProduct.style.left = cartCoordinates?.left + "px";
      clonedProduct.style.top = cartCoordinates?.top + "px";
    }, 10);

    // Удаление копии товара после завершения анимации
    setTimeout(() => {
      document.body.removeChild(clonedProduct);
    }, 500);
  };

  return (
    <div className="good-container">
      <div className="good-img">
        <img src={good.img} alt={good.title} />
      </div>
      <div className="good-wrap-content">
        <div className="good-info">
          <p className="good-name">{good.title}</p>
          <div className="good-container-price">
            <div className="good-price">
              <span>{good.price}</span>
              <MdOutlineCurrencyRuble />
            </div>
            {good.discount && (
              <div className="good-discount">
                <span>{good.discount}</span>
                <MdOutlineCurrencyRuble />
              </div>
            )}
          </div>
        </div>
        <div className="block-buy">
          <div className="container-rate">
            <div>
              <img src="src/assets/star.svg" alt="star" />
            </div>
            <div>{good.rate}</div>
          </div>
          <button className="btn-buy" onClick={addProduct}>
            Купить
          </button>
        </div>
      </div>
    </div>
  );
};
