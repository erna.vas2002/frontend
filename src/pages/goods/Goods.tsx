import { GoodsContext } from "../../context/GoodsContext/GoodsContext";
import { GoodsList } from "./components/GoodsList/GoodsList";
import { useContext } from "react";

export const Goods = () => {
  const { state: goodsState } = useContext(GoodsContext);

  return (
    <div className="goods-container">
      {goodsState.goods.types.map((type) => (
        <div key={type}>
          <h3 className="good-type">{type}</h3>
          <GoodsList type={type} />
        </div>
      ))}
    </div>
  );
};
