import { Routes, Route } from "react-router-dom";
import { Goods } from "../pages/goods/Goods";
import { CartGoods } from "../pages/cart-goods/CartGoods";

export const Router = () => {
  return (
    <Routes>
      <Route element={<Goods />} path="/" />
      <Route element={<CartGoods />} path="/cart-goods" />
    </Routes>
  );
};
