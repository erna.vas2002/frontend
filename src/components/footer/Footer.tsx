export const Footer = () => {
  return (
    <div className="footer-container">
      <div className="footer-inner">
        <div>
          <img src="src\assets\logo-footer.png" alt="QUPICK" />
        </div>
        <div>
          <ul className="footer-list">
            <li>Избранное</li>
            <li>Корзина</li>
            <li>Контакты</li>
          </ul>
        </div>
        <div>
          <ul>
            <li>Условия сервиса</li>
            <li>
              <ul className="footer-language-list">
                <li>
                  <img src="src\assets\icons\RU.svg" alt="язык" />
                </li>
                <li>
                  <img src="src\assets\Рус.png" alt="русский" />
                </li>
                <li>
                  <img src="src\assets\Eng.png" alt="английский" />
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <div>
          <ul className="footer-contacts-list">
            <li>
              <img src="src\assets\icons\VK.svg" alt="vk" />
            </li>
            <li>
              <img src="src\assets\icons\Telegram.svg" alt="telegram" />
            </li>
            <li>
              <img src="src\assets\icons\Whatsapp.svg" alt="whatsapp" />
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};
