import logo from "../../assets/logo.png";
import favorites from "../../assets/favorites.svg";
import cart from "../../assets/cart.svg";
import { Link } from "react-router-dom";
import { useContext } from "react";
import { CartContext } from "../../context/CartContext/CartContext";

export const Header = () => {
  const { state: cartProductsState } = useContext(CartContext);

  const getCountProducts = () => {
    return cartProductsState.productsInCart.reduce(
      (acc, cur) => acc + cur.count,
      0
    );
  };

  return (
    <div className="header-container">
      <div className="header-inner">
        <div>
          <Link to="/">
            <img src={logo} alt="QPICK" />
          </Link>
        </div>
        <ul className="header-menu">
          <li className="favorites">
            <img src={favorites} alt="favorites" />
            <div className="count">0</div>
          </li>
          <Link to="/cart-goods" className="cart">
            <img src={cart} alt="cart" />
            <div className="count">{getCountProducts()}</div>
          </Link>
        </ul>
      </div>
    </div>
  );
};
