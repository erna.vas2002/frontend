import React, { createContext, useReducer } from "react";
import { Action } from "./Types.ts";
import { State } from "./Types.ts";

const initialState: State = {
  productsInCart: [],
};

const reducer = (state: State, action: Action) => {
  switch (action.type) {
    case "ADD_PRODUCT_CART":
      return {
        ...state,
        productsInCart: [
          ...state.productsInCart,
          { count: 0, productInCart: { ...action.payload } },
        ],
      };
    case "DELETE_PRODUCT_CART":
      return {
        ...state,
        productsInCart: state.productsInCart.filter(
          (elem) => elem.productInCart.id !== action.payload
        ),
      };
    case "INCREMENT_NUMBER_PRODUCTS":
      return {
        ...state,
        productsInCart: state.productsInCart.map((item) => {
          if (item.productInCart.id === action.payload) {
            return {
              ...item,
              count: item.count + 1,
            };
          }
          return item;
        }),
      };
    case "DECREMENT_NUMBER_PRODUCTS":
      return {
        ...state,
        productsInCart: state.productsInCart.map((item) => {
          if (item.productInCart.id === action.payload && item.count > 1) {
            return {
              ...item,
              count: item.count - 1,
            };
          }
          return item;
        }),
      };
    default:
      return state;
  }
};

export const CartContext = createContext<{
  state: State;
  dispatch: React.Dispatch<Action>;
}>({
  state: initialState,
  dispatch: () => null,
});

export const CartProvider: React.FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <CartContext.Provider value={{ state, dispatch }}>
      {children}
    </CartContext.Provider>
  );
};
