import { productType } from "../../types/GoodsInCartType.ts";
import { GoodsInCartType } from "../../types/GoodsInCartType.ts";

export type State = {
  productsInCart: GoodsInCartType[];
};

export type Action =
  | { type: "SET_PRODUCT_CART"; payload: productType[] }
  | { type: "ADD_PRODUCT_CART"; payload: productType }
  | { type: "DELETE_PRODUCT_CART"; payload: number }
  | { type: "INCREMENT_NUMBER_PRODUCTS"; payload: number }
  | { type: "DECREMENT_NUMBER_PRODUCTS"; payload: number };
