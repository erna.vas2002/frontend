import { productType } from "../../types/GoodsInCartType.ts";
import { Action } from "./Types.ts";

export const setProductCart = (products: productType[]): Action => ({
  type: "SET_PRODUCT_CART",
  payload: products,
});

export const addProductCart = (product: productType): Action => ({
  type: "ADD_PRODUCT_CART",
  payload: product,
});

export const deleteProductCart = (id: number): Action => ({
  type: "DELETE_PRODUCT_CART",
  payload: id,
});

export const incrementProducts = (id: number): Action => ({
  type: "INCREMENT_NUMBER_PRODUCTS",
  payload: id,
});

export const decrementProducts = (id: number): Action => ({
  type: "DECREMENT_NUMBER_PRODUCTS",
  payload: id,
});
