import { GoodsType } from "../../types/GoodsType";
import { Action } from "./Types";

export const setGoods = (goods: GoodsType): Action => ({
  type: "SET_GOODS",
  payload: goods,
});
