import { createContext, useEffect } from "react";
import { useReducer } from "react";
import fetchGoods from "../../mocks/goods.json";
import { setGoods } from "./GoodsAction";
import { Action, State } from "./Types";

const initialState: State = {
  goods: {
    types: [],
    HeadphoneCategory: {},
  },
};

const reducer = (state: State, action: Action) => {
  switch (action.type) {
    case "SET_GOODS":
      return { ...state, goods: action.payload };
    default:
      return state;
  }
};

export const GoodsContext = createContext<{
  state: State;
  dispatch: React.Dispatch<Action>;
}>({
  state: initialState,
  dispatch: () => null,
});

export const GoodsProvider: React.FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    dispatch(setGoods(fetchGoods));
  }, []);

  return (
    <GoodsContext.Provider value={{ state, dispatch }}>
      {children}
    </GoodsContext.Provider>
  );
};
