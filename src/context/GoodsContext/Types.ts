import { GoodsType } from "../../types/GoodsType";

export type State = {
  goods: GoodsType;
};

export type Action = { type: "SET_GOODS"; payload: GoodsType };
