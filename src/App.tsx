import { Header } from "./components/header/Header";
import { GoodsProvider } from "./context/GoodsContext/GoodsContext";
import { CartProvider } from "./context/CartContext/CartContext";
import { Footer } from "./components/footer/Footer";
import { Router } from "./ui/Router";

function App() {
  return (
    <div className="app-container">
      <GoodsProvider>
        <CartProvider>
          <Header />
          <Router />
          <Footer />
        </CartProvider>
      </GoodsProvider>
    </div>
  );
}

export default App;

//  element={<CartProvider><CartGoods/></CartProvider>}
