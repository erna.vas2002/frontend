export interface Headphone {
  id: number;
  img: string;
  title: string;
  price: number;
  discount?: number;
  rate: number;
}

export interface HeadphoneCategory {
  [key: string]: Headphone[];
}

export interface GoodsType {
  types: string[];
  HeadphoneCategory: HeadphoneCategory;
}
