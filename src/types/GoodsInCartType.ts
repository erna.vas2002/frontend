export interface productType {
  id: number;
  img: string;
  title: string;
  price: number;
  discount?: number;
}

export interface GoodsInCartType {
  count: number;
  productInCart: productType;
}
